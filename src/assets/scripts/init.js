$('#searchText').text("Please wait...");
showInfo('info_start');

var msg = new SpeechSynthesisUtterance("Hack for voice not setting for first time");

msg.voice = speechSynthesis.getVoices().filter(
    function(voice) { 
      return voice.name == "Samantha"; 
    })[0];
msg.volume = 0;
speechSynthesis.speak(msg);

var create_email = false;
var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;
if (!('webkitSpeechRecognition' in window)) {
  upgrade();
} else {
  start_button.style.display = 'inline-block';
  var recognition = new webkitSpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = true;

  recognition.onstart = function() {
    recognizing = true;
    showInfo('info_speak_now');
    start_img.src = 'images/mic-animate.gif';
  };

  recognition.onerror = function(event) {
    if (event.error == 'no-speech') {
      start_img.src = 'images/mic.gif';
      showInfo('info_no_speech');
      ignore_onend = true;
    }
    if (event.error == 'audio-capture') {
      start_img.src = 'images/mic.gif';
      showInfo('info_no_microphone');
      ignore_onend = true;
    }
    if (event.error == 'not-allowed') {
      if (event.timeStamp - start_timestamp < 100) {
        showInfo('info_blocked');
      } else {
        showInfo('info_denied');
      }
      ignore_onend = true;
    }
  };

  recognition.onend = function() {
    recognizing = false;
    if (ignore_onend) {
      return;
    }
    start_img.src = 'images/mic.gif';
    if (!final_transcript) {
      showInfo('info_start');
      return;
    }
    showInfo('');
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
      var range = document.createRange();
      range.selectNode(document.getElementById('final_span'));
      window.getSelection().addRange(range);
    }
  };

  recognition.onresult = function(event) {
    var interim_transcript = '';
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;

        $('#searchText').empty();
        $('#searchText').text(final_transcript);
        speechText(final_transcript);
        final_transcript = "";

        recognition.stop();
        $(".angel-overlay, .helloangel").fadeOut("fast").removeClass("active");
        $(".angel-listen").removeClass("recording");
      } else {
        interim_transcript += event.results[i][0].transcript;
      }
    }
    final_transcript = capitalize(final_transcript);
    final_span.innerHTML = linebreak(final_transcript);
    interim_span.innerHTML = linebreak(interim_transcript);
  };
}

function upgrade() {
  start_button.style.visibility = 'hidden';
  showInfo('info_upgrade');
}

var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
  return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}

var first_char = /\S/;
function capitalize(s) {
  return s.replace(first_char, function(m) { return m.toUpperCase(); });
}

function startButton(event) {
  if (recognizing) {
    recognition.stop();
    return;
  }
  final_transcript = '';
  recognition.start();
  ignore_onend = false;
  final_span.innerHTML = '';
  interim_span.innerHTML = '';
  start_img.src = 'images/mic-slash.gif';
  showInfo('info_allow');
  start_timestamp = event.timeStamp;
}

function showInfo(s) {
  if (s) {
    for (var child = info.firstChild; child; child = child.nextSibling) {
      if (child.style) {
        child.style.display = child.id == s ? 'inline' : 'none';
      }
    }
    info.style.visibility = 'visible';
  } else {
    info.style.visibility = 'hidden';
  }
}
/* Web speech code end*/


/* Geolocation code on load start */
var geolocationPosition,
  zipCode = 43068;

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
  geolocationPosition = position;
    console.log("Geolocation Latitude: " + position.coords.latitude + 
    "Longitude: " + position.coords.longitude); 
    getZipcode();
    $('#searchText').text("Angel is ready");
}
getLocation();//Get geolocation

function getZipcode() {
  var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+geolocationPosition.coords.latitude+","+geolocationPosition.coords.longitude+"&key=AIzaSyCuubF3U5yd0MErIx8uamF01Z0fBEBowio"; 

  $.getJSON(url, function(data) {
    if(data.results[0] !== undefined){
       for(var i=0; i < data.results[0].address_components.length; i++)
      {
          var component = data.results[0].address_components[i];
          if(component.types[0] == "postal_code")
          {
              console.log("Your ZipCode: "+component.long_name);
              zipCode = component.long_name;
          }
      }
    }
  });

}

/* Geolocation code on load End */

/* Process speech text start */

var AngelCommands = {
    "data": [
        {
            "command": "store",
            "method": "GetStores",
            "subcommands": [
                {
                    "command": "direction",
                    "method": "GetDirections"
                },
                {
                    "command": "hours",
                    "method": "GetStoreHours"
                }
            ]
        },
        {
            "command": "product",
            "method": "SearchProducts",
            "subcommands": [
                {
                    "command": "option",
                    "method": "ProductOptions"
                }
            ]
        },
        {
            "command": "filter",
            "method": "Filters",
            "subcommands": [
                {
                    "command": "color",
                    "method": "FilterColor"
                },
                {
                    "command": "brand",
                    "method": "SelectBrand"
                },
                {
                    "command": "category",
                    "method": "SelectCategory"
                }
            ]
        },
        {
            "command": "select",
            "method": "Filters",
            "subcommands": [
                {
                    "command": "color",
                    "method": "FilterColor"
                },
                {
                    "command": "brand",
                    "method": "SelectBrand"
                },
                {
                    "command": "category",
                    "method": "SelectCategory"
                }
            ]
        },
        {
            "command": "sort",
            "method": "SortProducts"
        },
        {
            "command": "all",
            "method": "ShowProducts"
        },
        {
            "command": "name",
            "method": "AngelIntroduction"
        },
        {
            "command": "weather",
            "method": "GetWeather"
        },
        {
            "command": "thank",
            "method": "Bye"
        },
        {
            "command": "test",
            "method": "TestCommand"
        },

    ]
};

//Angel Search Engine
function speechText (searchText) {
  //Proceed only if search text has Angel
  // if(searchText.toLowerCase().includes("angel")){
  var words = searchText.split(" ");//Split searchtext into words

  //Find if search text has any commands and get the function to execute
  var commandResults = findCommand(words); 
  console.log(commandResults);

  if(commandResults){
    window[commandResults.method](words);
  }else{
    AgentSpeak("I did not find any results for this search");
  }
  // }
}

//Find if Angel engine supports any commands from the list of words
function findCommand (words) {
  var commandObj = searchArray(words, AngelCommands.data),
      commandObjSub;

  if(commandObj) {
    if(commandObj.subcommands && commandObj.subcommands.length > 0){
        commandObjSub = searchArray(words,commandObj.subcommands);
        if(commandObjSub){
          return commandObjSub;
        }else{
          return commandObj;
        }
    }
    else{
      return commandObj;
    }
  }

  return null;
}

//Searches commands for the command
function searchArray (words, commandArray) {

  for (var i=0; i < words.length; i++) {
    for (var j = 0; j < commandArray.length; j++) {

      if (words[i].toLowerCase().search(commandArray[j].command) != -1) {
        console.log("Command found: "+commandArray[j].command);

        return commandArray[j];
      }
    }
  }
}

/* Process speech text end */

/* General Api start */
function AngelIntroduction (param) {
  AgentSpeak("My name is Angel. I am a Victoria Secret online assistant.");
  console.log("Angel Introduction "+param);
}

function AgentSpeak (speakText) {
  speechSynthesis.cancel();
  var msg = new SpeechSynthesisUtterance(speakText);

  msg.voice = speechSynthesis.getVoices().filter(
    function(voice) { 
      return voice.name == "Samantha"; 
    })[0];
  speechSynthesis.speak(msg);
}

function Bye() {
  $("#angelsearchresults").empty();
  AgentSpeak("You're most welcome! I'm a fancy prototype, don't you think? Thank you every one and have a wonderful day!");
}

function TestCommand() {
  AgentSpeak("Test passed!");
}
/* General Api END */


