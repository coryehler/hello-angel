var storeData = { data: [
    {
        "storeId": "1587",
        "mallName": "Easton Town Center",
        "streetAddress": "4070 The Strand East",
        "city": "Columbus",
        "state": "OH",
        "stateName": "OHIO",
        "postalCode": "43219",
        "country": "US",
        "phone": "(614) 337-1824",
        "storeType": "09",
        "storeStatus": "01",
        "openingDate": "08/09/2012",
        "closingDate": "09/19/9999",
        "latitudeDeg": "40.051713",
        "longitudeDeg": "-82.915447",
        "latitudeRad": "0.6990342629138157",
        "longitudeRad": "-1.4471475509128549",
        "hoursEffectiveDate": null,
        "hoursOpenTimeSun": "11:00 AM",
        "hoursCloseTimeSun": "6:00 PM",
        "hoursOpenFlagSun": "Y",
        "hoursOpenTimeMon": "10:00 AM",
        "hoursCloseTimeMon": "9:00 PM",
        "hoursOpenFlagMon": "Y",
        "hoursOpenTimeTue": "10:00 AM",
        "hoursCloseTimeTue": "9:00 PM",
        "hoursOpenFlagTue": "Y",
        "hoursOpenTimeWed": "10:00 AM",
        "hoursCloseTimeWed": "9:00 PM",
        "hoursOpenFlagWed": "Y",
        "hoursOpenTimeThu": "10:00 AM",
        "hoursCloseTimeThu": "9:00 PM",
        "hoursOpenFlagThu": "Y",
        "hoursOpenTimeFri": "10:00 AM",
        "hoursCloseTimeFri": "10:00 PM",
        "hoursOpenFlagFri": "Y",
        "hoursOpenTimeSat": "10:00 AM",
        "hoursCloseTimeSat": "10:00 PM",
        "hoursOpenFlagSat": "Y",
        "extractDate": null,
        "loadDate": null,
        "assortmentInfo": [
            {
                "assortmentType": "PINK"
            }
        ],
        "messageInfo": []
    },
    {
        "storeId": "770",
        "mallName": "Mall at Tuttle Crossing",
        "streetAddress": "5043 Tuttle Crossing Boulevard",
        "city": "Dublin",
        "state": "OH",
        "stateName": "OHIO",
        "postalCode": "43016",
        "country": "US",
        "phone": "(614) 718-2854",
        "storeType": "06",
        "storeStatus": "01",
        "openingDate": "07/24/1997",
        "closingDate": "09/19/9999",
        "latitudeDeg": "40.072865",
        "longitudeDeg": "-83.130138",
        "latitudeRad": "0.6994034349571975",
        "longitudeRad": "-1.450894615737254",
        "hoursEffectiveDate": null,
        "hoursOpenTimeSun": "11:00 AM",
        "hoursCloseTimeSun": "6:00 PM",
        "hoursOpenFlagSun": "Y",
        "hoursOpenTimeMon": "10:00 AM",
        "hoursCloseTimeMon": "9:00 PM",
        "hoursOpenFlagMon": "Y",
        "hoursOpenTimeTue": "10:00 AM",
        "hoursCloseTimeTue": "9:00 PM",
        "hoursOpenFlagTue": "Y",
        "hoursOpenTimeWed": "10:00 AM",
        "hoursCloseTimeWed": "9:00 PM",
        "hoursOpenFlagWed": "Y",
        "hoursOpenTimeThu": "10:00 AM",
        "hoursCloseTimeThu": "9:00 PM",
        "hoursOpenFlagThu": "Y",
        "hoursOpenTimeFri": "10:00 AM",
        "hoursCloseTimeFri": "9:00 PM",
        "hoursOpenFlagFri": "Y",
        "hoursOpenTimeSat": "10:00 AM",
        "hoursCloseTimeSat": "9:00 PM",
        "hoursOpenFlagSat": "Y",
        "extractDate": null,
        "loadDate": null,
        "assortmentInfo": [
            {
                "assortmentType": "BEAUTY"
            },
            {
                "assortmentType": "LINGERIE"
            },
            {
                "assortmentType": "PINK"
            },
            {
                "assortmentType": "SWIM"
            },
            {
                "assortmentType": "VSX"
            }
        ],
        "messageInfo": []
    },
    {
        "storeId": "709",
        "mallName": "Eastdale",
        "streetAddress": "1067 Eastdale Mall",
        "city": "Montgomery",
        "state": "AL",
        "stateName" : "Alabama",
        "postalCode": "36117",
        "country": "US",
        "phone": "(334) 244-4120",
        "storeType": "06",
        "storeStatus": "01",
        "openingDate": "06/26/1996",
        "closingDate": "09/19/9999",
        "latitudeDeg": "32.386",
        "longitudeDeg": "-86.2041",
        "latitudeRad": "0.5652423315508837",
        "longitudeRad": "-1.5045453737184438",
        "hoursEffectiveDate": null,
        "hoursOpenTimeSun": "1:00 PM",
        "hoursCloseTimeSun": "6:00 PM",
        "hoursOpenFlagSun": "Y",
        "hoursOpenTimeMon": "10:00 AM",
        "hoursCloseTimeMon": "9:00 PM",
        "hoursOpenFlagMon": "Y",
        "hoursOpenTimeTue": "10:00 AM",
        "hoursCloseTimeTue": "9:00 PM",
        "hoursOpenFlagTue": "Y",
        "hoursOpenTimeWed": "10:00 AM",
        "hoursCloseTimeWed": "9:00 PM",
        "hoursOpenFlagWed": "Y",
        "hoursOpenTimeThu": "10:00 AM",
        "hoursCloseTimeThu": "9:00 PM",
        "hoursOpenFlagThu": "Y",
        "hoursOpenTimeFri": "10:00 AM",
        "hoursCloseTimeFri": "9:00 PM",
        "hoursOpenFlagFri": "Y",
        "hoursOpenTimeSat": "10:00 AM",
        "hoursCloseTimeSat": "9:00 PM",
        "hoursOpenFlagSat": "Y",
        "extractDate": null,
        "loadDate": null,
        "assortmentInfo": [
            {
                "assortmentType": "BEAUTY"
            },
            {
                "assortmentType": "LINGERIE"
            },
            {
                "assortmentType": "PINK"
            },
            {
                "assortmentType": "VSX"
            }
        ],
        "messageInfo": []
    },
{
        "storeId": "1489",
        "mallName": "Polaris Fashion Place",
        "streetAddress": "1500 Polaris Parkway",
        "city": "Columbus",
        "state": "OH",
        "stateName": "Ohio",
        "postalCode": "43240",
        "country": "US",
        "phone": "(614) 781-0806",
        "storeType": "99",
        "storeStatus": "01",
        "openingDate": "10/30/2008",
        "closingDate": "09/19/9999",
        "latitudeDeg": "40.149001",
        "longitudeDeg": "-82.979665",
        "latitudeRad": "0.7007322588364958",
        "longitudeRad": "-1.4482683664519005",
        "hoursEffectiveDate": null,
        "hoursOpenTimeSun": "12:00 PM",
        "hoursCloseTimeSun": "6:00 PM",
        "hoursOpenFlagSun": "Y",
        "hoursOpenTimeMon": "10:00 AM",
        "hoursCloseTimeMon": "9:00 PM",
        "hoursOpenFlagMon": "Y",
        "hoursOpenTimeTue": "10:00 AM",
        "hoursCloseTimeTue": "9:00 PM",
        "hoursOpenFlagTue": "Y",
        "hoursOpenTimeWed": "10:00 AM",
        "hoursCloseTimeWed": "9:00 PM",
        "hoursOpenFlagWed": "Y",
        "hoursOpenTimeThu": "10:00 AM",
        "hoursCloseTimeThu": "9:00 PM",
        "hoursOpenFlagThu": "Y",
        "hoursOpenTimeFri": "10:00 AM",
        "hoursCloseTimeFri": "9:30 PM",
        "hoursOpenFlagFri": "Y",
        "hoursOpenTimeSat": "10:00 AM",
        "hoursCloseTimeSat": "9:30 PM",
        "hoursOpenFlagSat": "Y",
        "extractDate": null,
        "loadDate": null,
        "assortmentInfo": [],
        "messageInfo": []
    } ,

   {
        "storeId": "699",
        "mallName": "Sandusky",
        "streetAddress": "4314 Milan Road",
        "city": "Sandusky",
        "state": "OH",
        "stateName": "Ohio",
        "postalCode": "44870",
        "country": "US",
        "phone": "(419) 621-5520",
        "storeType": "06",
        "storeStatus": "01",
        "openingDate": "09/13/1995",
        "closingDate": "09/19/9999",
        "latitudeDeg": "41.4247",
        "longitudeDeg": "-82.6647",
        "latitudeRad": "0.7229974066508951",
        "longitudeRad": "-1.4427711901733564",
        "hoursEffectiveDate": null,
        "hoursOpenTimeSun": "11:00 AM",
        "hoursCloseTimeSun": "6:00 PM",
        "hoursOpenFlagSun": "Y",
        "hoursOpenTimeMon": "10:00 AM",
        "hoursCloseTimeMon": "9:00 PM",
        "hoursOpenFlagMon": "Y",
        "hoursOpenTimeTue": "10:00 AM",
        "hoursCloseTimeTue": "9:00 PM",
        "hoursOpenFlagTue": "Y",
        "hoursOpenTimeWed": "10:00 AM",
        "hoursCloseTimeWed": "9:00 PM",
        "hoursOpenFlagWed": "Y",
        "hoursOpenTimeThu": "10:00 AM",
        "hoursCloseTimeThu": "9:00 PM",
        "hoursOpenFlagThu": "Y",
        "hoursOpenTimeFri": "10:00 AM",
        "hoursCloseTimeFri": "9:00 PM",
        "hoursOpenFlagFri": "Y",
        "hoursOpenTimeSat": "10:00 AM",
        "hoursCloseTimeSat": "9:00 PM",
        "hoursOpenFlagSat": "Y",
        "extractDate": null,
        "loadDate": null,
        "assortmentInfo": [
            {
                "assortmentType": "BEAUTY"
            },
            {
                "assortmentType": "LINGERIE"
            },
            {
                "assortmentType": "PINK"
            },
            {
                "assortmentType": "SWIM"
            },
            {
                "assortmentType": "VSX"
            }
        ],
        "messageInfo": []
    } ,
  {
        "storeId": "137",
        "mallName": "Dayton Mall",
        "streetAddress": "2700 Miamisburg-Centerville",
        "city": "Dayton",
        "state": "OH",
        "stateName": "Ohio",
        "postalCode": "45459",
        "country": "US",
        "phone": "(937) 436-7428",
        "storeType": "06",
        "storeStatus": "01",
        "openingDate": "09/24/1986",
        "closingDate": "09/19/9999",
        "latitudeDeg": "39.634372",
        "longitudeDeg": "-84.216822",
        "latitudeRad": "0.69175028836025",
        "longitudeRad": "-1.4698608294659958",
        "hoursEffectiveDate": null,
        "hoursOpenTimeSun": "12:00 PM",
        "hoursCloseTimeSun": "6:00 PM",
        "hoursOpenFlagSun": "Y",
        "hoursOpenTimeMon": "10:00 AM",
        "hoursCloseTimeMon": "9:00 PM",
        "hoursOpenFlagMon": "Y",
        "hoursOpenTimeTue": "10:00 AM",
        "hoursCloseTimeTue": "9:00 PM",
        "hoursOpenFlagTue": "Y",
        "hoursOpenTimeWed": "10:00 AM",
        "hoursCloseTimeWed": "9:00 PM",
        "hoursOpenFlagWed": "Y",
        "hoursOpenTimeThu": "10:00 AM",
        "hoursCloseTimeThu": "9:00 PM",
        "hoursOpenFlagThu": "Y",
        "hoursOpenTimeFri": "10:00 AM",
        "hoursCloseTimeFri": "9:00 PM",
        "hoursOpenFlagFri": "Y",
        "hoursOpenTimeSat": "10:00 AM",
        "hoursCloseTimeSat": "9:00 PM",
        "hoursOpenFlagSat": "Y",
        "extractDate": null,
        "loadDate": null,
        "assortmentInfo": [
            {
                "assortmentType": "BEAUTY"
            },
            {
                "assortmentType": "LINGERIE"
            },
            {
                "assortmentType": "PINK"
            },
            {
                "assortmentType": "SWIM"
            },
            {
                "assortmentType": "VSX"
            }
        ],
        "messageInfo": []
    } ,
{
    "storeId": "150",
    "mallName": "57th Street",
    "streetAddress": "34 E 57th Street",
    "city": "New York",
    "state": "New York",
    "stateName": "New York",
    "postalCode": "10022",
    "country": "US",
    "phone": "(212) 758-5592",
    "storeType": "07",
    "storeStatus": "01",
    "openingDate": "11/29/1986",
    "closingDate": "09/19/9999",
    "latitudeDeg": "40.762303",
    "longitudeDeg": "-73.972322",
    "latitudeRad": "0.7114363980455622",
    "longitudeRad": "-1.291060574245437",
    "hoursEffectiveDate": null,
    "hoursOpenTimeSun": "12:00 PM",
    "hoursCloseTimeSun": "7:00 PM",
    "hoursOpenFlagSun": "Y",
    "hoursOpenTimeMon": "10:00 AM",
    "hoursCloseTimeMon": "8:00 PM",
    "hoursOpenFlagMon": "Y",
    "hoursOpenTimeTue": "10:00 AM",
    "hoursCloseTimeTue": "8:00 PM",
    "hoursOpenFlagTue": "Y",
    "hoursOpenTimeWed": "10:00 AM",
    "hoursCloseTimeWed": "8:00 PM",
    "hoursOpenFlagWed": "Y",
    "hoursOpenTimeThu": "10:00 AM",
    "hoursCloseTimeThu": "8:00 PM",
    "hoursOpenFlagThu": "Y",
    "hoursOpenTimeFri": "10:00 AM",
    "hoursCloseTimeFri": "8:00 PM",
    "hoursOpenFlagFri": "Y",
    "hoursOpenTimeSat": "10:00 AM",
    "hoursCloseTimeSat": "8:00 PM",
    "hoursOpenFlagSat": "Y",
    "extractDate": null,
    "loadDate": null,
    "assortmentInfo": [
        {
            "assortmentType": "BEAUTY"
        },
        {
            "assortmentType": "LINGERIE"
        },
        {
            "assortmentType": "SWIM"
        },
        {
            "assortmentType": "VSX"
        }
    ],
    "messageInfo": [
        
    ]
} ,
{
    "storeId": "1446",
    "mallName": "Lexington Avenue",
    "streetAddress": "722 Lexington Avenue",
    "city": "New York",
    "state": "New York",
    "stateName": "NY",
    "postalCode": "10022",
    "country": "US",
    "phone": "(212) 230-1647",
    "storeType": "06",
    "storeStatus": "01",
    "openingDate": "11/15/2008",
    "closingDate": "09/19/9999",
    "latitudeDeg": "40.7619",
    "longitudeDeg": "-73.9691",
    "latitudeRad": "0.7114293643686765",
    "longitudeRad": "-1.2910043397369375",
    "hoursEffectiveDate": null,
    "hoursOpenTimeSun": "11:00 AM",
    "hoursCloseTimeSun": "7:00 PM",
    "hoursOpenFlagSun": "Y",
    "hoursOpenTimeMon": "10:00 AM",
    "hoursCloseTimeMon": "9:00 PM",
    "hoursOpenFlagMon": "Y",
    "hoursOpenTimeTue": "10:00 AM",
    "hoursCloseTimeTue": "9:00 PM",
    "hoursOpenFlagTue": "Y",
    "hoursOpenTimeWed": "10:00 AM",
    "hoursCloseTimeWed": "9:00 PM",
    "hoursOpenFlagWed": "Y",
    "hoursOpenTimeThu": "10:00 AM",
    "hoursCloseTimeThu": "9:00 PM",
    "hoursOpenFlagThu": "Y",
    "hoursOpenTimeFri": "10:00 AM",
    "hoursCloseTimeFri": "9:00 PM",
    "hoursOpenFlagFri": "Y",
    "hoursOpenTimeSat": "10:00 AM",
    "hoursCloseTimeSat": "9:00 PM",
    "hoursOpenFlagSat": "Y",
    "extractDate": null,
    "loadDate": null,
    "assortmentInfo": [
        {
            "assortmentType": "BEAUTY"
        },
        {
            "assortmentType": "LINGERIE"
        },
        {
            "assortmentType": "SWIM"
        },
        {
            "assortmentType": "VSX"
        }
    ],
    "messageInfo": []
} ,
  {
        "storeId": "1124",
        "mallName": "Jacksonville",
        "streetAddress": "340 Jacksonville Mall",
        "city": "Jacksonville",
        "state": "NC",
        "stateName": "North Carolina",
        "postalCode": "28546",
        "country": "US",
        "phone": "(910) 353-3838",
        "storeType": "07",
        "storeStatus": "01",
        "openingDate": "06/07/2000",
        "closingDate": "09/19/9999",
        "latitudeDeg": "34.7693",
        "longitudeDeg": "-77.3748",
        "latitudeRad": "0.6068387636136644",
        "longitudeRad": "-1.3504450180721084",
        "hoursEffectiveDate": null,
        "hoursOpenTimeSun": "12:00 PM",
        "hoursCloseTimeSun": "6:00 PM",
        "hoursOpenFlagSun": "Y",
        "hoursOpenTimeMon": "10:00 AM",
        "hoursCloseTimeMon": "9:00 PM",
        "hoursOpenFlagMon": "Y",
        "hoursOpenTimeTue": "10:00 AM",
        "hoursCloseTimeTue": "9:00 PM",
        "hoursOpenFlagTue": "Y",
        "hoursOpenTimeWed": "10:00 AM",
        "hoursCloseTimeWed": "9:00 PM",
        "hoursOpenFlagWed": "Y",
        "hoursOpenTimeThu": "10:00 AM",
        "hoursCloseTimeThu": "9:00 PM",
        "hoursOpenFlagThu": "Y",
        "hoursOpenTimeFri": "10:00 AM",
        "hoursCloseTimeFri": "9:00 PM",
        "hoursOpenFlagFri": "Y",
        "hoursOpenTimeSat": "10:00 AM",
        "hoursCloseTimeSat": "9:00 PM",
        "hoursOpenFlagSat": "Y",
        "extractDate": null,
        "loadDate": null,
        "assortmentInfo": [
            {
                "assortmentType": "BEAUTY"
            },
            {
                "assortmentType": "LINGERIE"
            },
            {
                "assortmentType": "PINK"
            },
            {
                "assortmentType": "VSX"
            }
        ],
        "messageInfo": []
    },

    {
        "storeId": "253",
        "mallName": "Eastern Hills",
        "streetAddress": "4545 Transit Road",
        "city": "Williamsville",
        "state": "NY",
        "stateName": "New York",
        "postalCode": "14221",
        "country": "US",
        "phone": "(716) 633-6160",
        "storeType": "07",
        "storeStatus": "01",
        "openingDate": "01/11/1988",
        "closingDate": "09/19/9999",
        "latitudeDeg": "42.9905",
        "longitudeDeg": "-78.6969",
        "latitudeRad": "0.7503257720786222",
        "longitudeRad": "-1.3735200161127255",
        "hoursEffectiveDate": null,
        "hoursOpenTimeSun": "12:00 PM",
        "hoursCloseTimeSun": "5:00 PM",
        "hoursOpenFlagSun": "Y",
        "hoursOpenTimeMon": "10:00 AM",
        "hoursCloseTimeMon": "9:00 PM",
        "hoursOpenFlagMon": "Y",
        "hoursOpenTimeTue": "10:00 AM",
        "hoursCloseTimeTue": "9:00 PM",
        "hoursOpenFlagTue": "Y",
        "hoursOpenTimeWed": "10:00 AM",
        "hoursCloseTimeWed": "9:00 PM",
        "hoursOpenFlagWed": "Y",
        "hoursOpenTimeThu": "10:00 AM",
        "hoursCloseTimeThu": "9:00 PM",
        "hoursOpenFlagThu": "Y",
        "hoursOpenTimeFri": "10:00 AM",
        "hoursCloseTimeFri": "9:00 PM",
        "hoursOpenFlagFri": "Y",
        "hoursOpenTimeSat": "10:00 AM",
        "hoursCloseTimeSat": "9:00 PM",
        "hoursOpenFlagSat": "Y",
        "extractDate": null,
        "loadDate": null,
        "assortmentInfo": [
            {
                "assortmentType": "BEAUTY"
            },
            {
                "assortmentType": "LINGERIE"
            },
            {
                "assortmentType": "PINK"
            },
            {
                "assortmentType": "VSX"
            }
        ],
        "messageInfo": []
    }

]};