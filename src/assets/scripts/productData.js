var productData = { data: [

	
	//VS Products 
	//Bras
	{
		"name": "Lace-trim Cutout Bra",
		"color": "pink",
		"brand": "victoria secret",
		"category": "bras",
		"price": "58.50",
		"rating": "5",
		"image": "https://dm.victoriassecret.com/product/404x539/V445001_OM_F.jpg"
	},
	{
		"name": "Add-1 1/2-Cups Bra",
		"color": "blue",
		"brand": "victoria secret",
		"category": "bras",
		"price": "52.50",
		"rating": "5",
		"image": "https://dm.victoriassecret.com/product/404x539/V451195_OM_F.jpg"
	},
	{
		"name": "Add-1 1/2-Cups Bra",
		"color": "green",
		"brand": "victoria secret",
		"category": "bras",
		"price": "49.50",
		"rating": "5",
		"image": "https://dm.victoriassecret.com/product/404x539/V556044_CROP1.jpg"
	},

	//Swim
	{
		"name": "Crochet-trim Teeny Top",
		"color": "green",
		"brand": "victoria secret",
		"category": "swim",
		"price": "44.50",
		"rating": "5",
		"image": "https://dm.victoriassecret.com/product/404x539/V555986.jpg"
	},
	{
		"name": "Gorgeous Crossback Top",
		"color": "blue",
		"brand": "victoria secret",
		"category": "swim",
		"price": "52.50",
		"rating": "5",
		"image": "https://dm.victoriassecret.com/product/404x539/V557022.jpg"
	},
	{
		"name": "The Hottie Halter",
		"color": "pink",
		"brand": "victoria secret",
		"category": "swim",
		"price": "54.50",
		"rating": "4.5",
		"image": "https://dm.victoriassecret.com/product/404x539/V556981.jpg"
	},

	//Sport
	{
		"name": "Knockout Sport Bra",
		"color": "pink",
		"brand": "victoria secret",
		"category": "sport",
		"price": "56.50",
		"rating": "4.4",
		"image": "https://dm.victoriassecret.com/product/404x539/V422301_OM_F.jpg"
	},
	{
		"name": "Knockout Sport Bra",
		"color": "blue",
		"brand": "victoria secret",
		"category": "sport",
		"price": "52.50",
		"rating": "4.6",
		"image": "https://dm.victoriassecret.com/product/404x539/V448486_OM_F.jpg"
	},
	{
		"name": "Knockout Sport Bra",
		"color": "green",
		"brand": "victoria secret",
		"category": "sport",
		"price": "54.50",
		"rating": "4.5",
		"image": "https://dm.victoriassecret.com/product/404x539/V448605_OM_F.jpg"
	},

	//PINK Products 
	//Bras
	{
		"name": "Medallion Lace Brallette",
		"color": "green",
		"brand": "pink",
		"category": "bras",
		"price": "34.95",
		"rating": "4.4",
		"image": "https://dm.victoriassecret.com/product/404x539/V448989_OM_F.jpg"
	},
	{
		"name": "Campus light Bra",
		"color": "pink",
		"brand": "pink",
		"category": "bras",
		"price": "39.95",
		"rating": "4.5",
		"image": "https://dm.victoriassecret.com/product/404x539/V435386_OM_F.jpg"
	},
	{
		"name": "Wear Everywhere Bra",
		"color": "blue",
		"brand": "pink",
		"category": "bras",
		"price": "32.95",
		"rating": "4.4",
		"image": "https://dm.victoriassecret.com/product/404x539/V454188_OM_F.jpg"
	},

	//Swim
	{
		"name": "Ruched Mini Bikini",
		"color": "pink",
		"brand": "pink",
		"category": "swim",
		"price": "44.50",
		"rating": "5",
		"image": "https://dm.victoriassecret.com/product/404x539/V427582.jpg"
	},

	//college and pro
	{
		"name": "OSU Gym Crew",
		"color": "Red",
		"brand": "pink",
		"category": "college",
		"price": "89.95",
		"rating": "4.4",
		"image": "https://dm.victoriassecret.com/product/404x539/V551208.jpg"
	},
	{
		"name": "OSU Ultimate Leggings",
		"color": "grey",
		"brand": "pink",
		"category": "college",
		"price": "72.50",
		"rating": "4.6",
		"image": "https://dm.victoriassecret.com/product/404x539/V553421.jpg"
	},
	{
		"name": "OSU sweat shirt",
		"color": "grey",
		"brand": "pink",
		"category": "college",
		"price": "114.50",
		"rating": "4.5",
		"image": "https://dm.victoriassecret.com/product/224x299/V552038.jpg"
	}

]};