var main = main || {};

main.Global = {
    
    $w: $(window),
    $b: $("body"),
    
    Init: function() {

    	var $targets = $(".angel-overlay, .helloangel");

    	$("button.hello-angel").on("click", function() {
    		$targets.fadeIn("fast").addClass("active");
    	});

    	// close overlay on escape keyup
    	$(document).keyup(function(e) {
		    if (e.keyCode === 27 && $targets.hasClass("active")) {
				$targets.fadeOut("fast").removeClass("active");	
		    }
		});

    	// start recording on spacebar keyup
		$(document).keyup(function(e) {
		    if (e.keyCode === 32 && $targets.hasClass("active")) {
		    	e.preventDefault();
				var $recordBtn = $(".angel-listen");
				
				if ($recordBtn.hasClass("recording")) {
					$recordBtn.removeClass("recording");
					showInfo('info_start');
				}
				else {
					$recordBtn.addClass("recording");
					startButton(event);
				}
		    }
		});

		$(".close-angel, .angel-overlay").on("click", function() {
			$targets.fadeOut("fast").removeClass("active");	
		});

		$(".angel-listen").on("click", function() {
			var $this = $(this);
			if ($this.hasClass("recording")) {
				$this.removeClass("recording");
				recognition.stop();
			}
			else {
				$this.addClass("recording");
				startButton(event);
			}
		});
    }
};

$(function() {
    main.Global.Init();
});