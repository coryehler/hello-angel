var citiesFilter = [ "columbus" , "dublin", "sandusky", "dayton", "new york", "jacksonville" ],
	statesFilter = [ "alabama" , "ohio", "new york", "north carolina" ];

/* Methods: Store start */

function GetStores (words) {
  var searchResults = $('#angelsearchresults'),
  	  citiesMatchedStores,
  	  statesMatchedStores,
  	  resultStoreData = [];
  
  //Get city matched stores / filter stores by cities
  citiesMatchedStores = getFilterMatchedStores(words,"city");
  statesMatchedStores = getFilterMatchedStores(words,"state");

  //All Filtered store Data
  if(citiesMatchedStores.length > 0 && statesMatchedStores.length > 0){
	resultStoreData = getUniqueStores(citiesMatchedStores,statesMatchedStores);
  } else if(citiesMatchedStores.length > 0){ // cities filetered data
	resultStoreData = citiesMatchedStores;
  } else if(statesMatchedStores.length > 0){ // states filteres data
	resultStoreData = statesMatchedStores;
  } else { //All store data
  	resultStoreData = storeData.data;
  }

  //Append to search results
  $('#angelsearchresults').empty();
  $('#angelsearchresults').append("<div id='map'></div><div id='right-panel'></div>");
  initMap(resultStoreData);

  AgentSpeak("Found "+resultStoreData.length+" stores");
}

//Takes words as parameters, find if filters are in words and then searches for stores with those matches
function getFilterMatchedStores(words, filterType) {
  var matchWordFilters,
  	  matchedStores = [];
      

  if(filterType === "city"){
  	matchWordFilters= matchedWordsInFilterArray(words, citiesFilter);
  } else if(filterType === "state"){
  	matchWordFilters= matchedWordsInFilterArray(words, statesFilter);
  }

  for(var i=0; i<matchWordFilters.length; i++){
  		if(filterType === "city"){
        for(var j=0; j<storeData.data.length; j++){
          if (String(storeData.data[j].city).toLowerCase() == String(matchWordFilters[i]).toLowerCase()) {
            matchedStores.push(storeData.data[j]);
          }
        }
  		}
      else if (filterType === "state") {
        for(var k=0; k<storeData.data.length; k++){
          if (String(storeData.data[k].stateName).toLowerCase() == String(matchWordFilters[i]).toLowerCase()) {
            matchedStores.push(storeData.data[k]);
          }
        }
  		}
  }

  return matchedStores;
}

function matchedWordsInFilterArray(wordsArray, filterArray){
	var matchedWords = [],
    voiceSearchText = wordsArray.join(' ').toLowerCase();

	for(var i=0; i<wordsArray.length; i++){
		for(var j=0; j<filterArray.length; j++){

			if ( (String(wordsArray[i]).toLowerCase() ===  String(filterArray[j]).toLowerCase() ) || (voiceSearchText.search(String(filterArray[j]).toLowerCase()) != -1) ){
        if(matchedWords.indexOf(filterArray[j]) === -1){
          matchedWords.push(filterArray[j]);
        }
			}

		}
	}

	return matchedWords;
}


//return unique list of stores
function getUniqueStores(array1 , array2) {
	var newArray = array1,
		elementFound;

	for(var i=0; i<array2.length; i++){
		elementFound = false;
    for(var j=0; j<array1.length; j++){
      if (String(array1[j].storeId).toLowerCase() == String(array2[i].storeId).toLowerCase()) {
        elementFound = true;
      }
    }

		if(elementFound === false){
			newArray.push(array2[i]);
		}
	}

	return newArray;
}
/* Methods: Store end */

function initMap(storeData) {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8
    }),
    bounds = new google.maps.LatLngBounds();

    //You are here marker
    createMarkers(geolocationPosition.coords.latitude, geolocationPosition.coords.longitude, map, bounds, true, storeData[i]);

    //create store markers
    for(var i=0; i<storeData.length; i++){
      createMarkers(parseFloat(storeData[i].latitudeDeg),parseFloat(storeData[i].longitudeDeg), map, bounds, false, storeData[i]);
    }

    //center the map to the geometric center of all markers
    map.setCenter(bounds.getCenter());

    map.fitBounds(bounds);

    //remove one zoom level to ensure no marker is on the edge.
    map.setZoom(map.getZoom()-1); 

    // set a minimum zoom 
    // if you got only 1 marker or all markers are on the same address map will be zoomed too much.
    if(map.getZoom()> 15){
     map.setZoom(15);
    }
  }

  function createMarkers(latitude, longitude, map, bounds, youHere, storeDataObj){
    var marker = new google.maps.Marker({
      map: map,
      draggable: false,
      animation: google.maps.Animation.DROP,
      position: {lat: latitude, lng: longitude}
    }),
    contentString,
    youHereInfowindow;
    infowindow = new google.maps.InfoWindow();

    if(youHere){
      contentString = "<p>You are here</p>";
      iconFile = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'; 
      marker.setIcon(iconFile);
      marker.setAnimation(google.maps.Animation.BOUNCE);

      youHereInfowindow = new google.maps.InfoWindow({
        content: contentString
      });
      youHereInfowindow.open(map, marker);
    }else{
      contentString = "<p><b>"+storeDataObj.mallName+"</b><br/>"+storeDataObj.streetAddress+"<br/>"+storeDataObj.city+", "+storeDataObj.state+"-"+storeDataObj.postalCode+
                      "<br>"+storeDataObj.phone+"</p>";

      marker.addListener('click', function() {
        infowindow.setContent(contentString);
        infowindow.open(map, marker);
      }); 
    }

    bounds.extend(marker.getPosition());
  }

  function GetStoreHours(words) {
    var matchedStores = [],
        elementFound = false,
        resultArray = [],
        voiceSearchText = words.join(' ').toLowerCase(),
        contentString = "";

    for(var i=0; i<words.length; i++){
      for(var k=0; k<storeData.data.length; k++){
         if (voiceSearchText.search(String(storeData.data[k].city).toLowerCase()) != -1) {
            matchedStores.push(storeData.data[k]);
          }else if (voiceSearchText.search(String(storeData.data[k].stateName).toLowerCase()) != -1) {
            matchedStores.push(storeData.data[k]);
          }else  if (voiceSearchText.search(String(storeData.data[k].mallName).toLowerCase()) != -1) {
            matchedStores.push(storeData.data[k]);
          }
      }
    }

    for(var j=0; j<matchedStores.length; j++){
      elementFound = false;
      for(var l=0; l<resultArray.length; l++){
         if (String(resultArray[l].storeId).toLowerCase() == String(matchedStores[j].storeId).toLowerCase()) {
            elementFound = true;
          }
      }

      if(elementFound === false){
        resultArray.push(matchedStores[j]);
      }
    }

    //Append to search results
    $('#angelsearchresults').empty();
    
    for(i=0; i<resultArray.length; i++){
      contentString += "<div class='store-info'><h4>" + resultArray[i].mallName+"</h4>"+resultArray[i].streetAddress+"<br/>"+resultArray[i].city+", "+resultArray[i].state+"-"+resultArray[i].postalCode+
                      "<br>"+resultArray[i].phone+"<br>";
      contentString += "<h4>Store Hours</h4>";
      contentString += "Sun "+resultArray[i].hoursOpenTimeSun+" to "+ resultArray[i].hoursCloseTimeSun+"<br/>";
      contentString += "Mon "+resultArray[i].hoursOpenTimeMon+" to "+ resultArray[i].hoursCloseTimeMon+"<br/>";
      contentString += "Tue "+resultArray[i].hoursOpenTimeTue+" to "+ resultArray[i].hoursCloseTimeTue+"<br/>";
      contentString += "Wed "+resultArray[i].hoursOpenTimeWed+" to "+ resultArray[i].hoursCloseTimeWed+"<br/>";
      contentString += "Thu "+resultArray[i].hoursOpenTimeThu+" to "+ resultArray[i].hoursCloseTimeThu+"<br/>";
      contentString += "Fri "+resultArray[i].hoursOpenTimeFri+" to "+ resultArray[i].hoursCloseTimeFri+"<br/>";
      contentString += "Sat "+resultArray[i].hoursOpenTimeSat+" to "+ resultArray[i].hoursCloseTimeSat+"<br/>";
      contentString += "</div>";
    }

    $('#angelsearchresults').append(contentString);

    AgentSpeak("Found "+resultArray.length+" stores");
  }




