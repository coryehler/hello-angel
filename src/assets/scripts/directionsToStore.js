function GetDirections(words) {
   var searchResults = $('#angelsearchresults'),
      matchedStores= [];
  
  //find words in store data mallname
  for(var i=0; i<words.length; i++){
    for(var k=0; k<storeData.data.length; k++){
      var mallArray = String(storeData.data[k].mallName).toLowerCase().split(" ");

      for(var j=0; j<mallArray.length; j++){
        if ( mallArray[j] === String(words[i]).toLowerCase() ) {
          matchedStores.push(storeData.data[k]);
          break;
        }
      }
    }
  }

  matchedStores = getUniqueMatchedStores(matchedStores);
  console.log(matchedStores);

  //Append to search results
  $('#angelsearchresults').empty();
  $('#angelsearchresults').append("<div id='map'></div><div id='right-panel'></div>");

  if(matchedStores.length > 0){
    getMap(matchedStores);

    AgentSpeak("Showing directions to "+matchedStores[0].mallName);
  }else {
    AgentSpeak("I did not find any results for this search");
  }
  
}

function getMap(storeData) {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8
    });

    //Directions
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var directionsService = new google.maps.DirectionsService();
    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById('right-panel'));

    calculateAndDisplayRoute(directionsService,directionsDisplay, storeData[0]);
    
  }

function calculateAndDisplayRoute(directionsService, directionsDisplay, storeDataObj) {
    
    directionsService.route({
      origin: new google.maps.LatLng(geolocationPosition.coords.latitude,geolocationPosition.coords.longitude),
      destination: new google.maps.LatLng(parseFloat(storeDataObj.latitudeDeg),parseFloat(storeDataObj.longitudeDeg)),
      travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
}

//return unique list of stores
function getUniqueMatchedStores(array1) {
  var newArray = [],
    elementFound;

  if(array1.length>0){
    newArray.push(array1[0]);
  }

  for(var i=0; i<array1.length; i++){
    elementFound = false;
    for(var j=0; j<newArray.length; j++){
      if (String(newArray[j].storeId).toLowerCase() == String(array1[i].storeId).toLowerCase()) {
        elementFound = true;
      }
    }

    if(elementFound === false){
      newArray.push(array1[i]);
    }
  }

  return newArray;
}