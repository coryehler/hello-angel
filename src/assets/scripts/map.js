function initMap(storeData) {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8
    }),
    bounds = new google.maps.LatLngBounds();

    //You are here marker
    createMarkers(geolocationPosition.coords.latitude, geolocationPosition.coords.longitude, map, bounds, true, storeData[i]);

    //create store markers
    for(var i=0; i<storeData.length; i++){
      createMarkers(parseFloat(storeData[i].latitudeDeg),parseFloat(storeData[i].longitudeDeg), map, bounds, false, storeData[i]);
    }

    //center the map to the geometric center of all markers
    map.setCenter(bounds.getCenter());

    map.fitBounds(bounds);

    //remove one zoom level to ensure no marker is on the edge.
    map.setZoom(map.getZoom()-1); 

    // set a minimum zoom 
    // if you got only 1 marker or all markers are on the same address map will be zoomed too much.
    if(map.getZoom()> 15){
     map.setZoom(15);
    }
  }

  function createMarkers(latitude, longitude, map, bounds, youHere, storeDataObj){
    var marker = new google.maps.Marker({
      map: map,
      draggable: false,
      animation: google.maps.Animation.DROP,
      position: {lat: latitude, lng: longitude}
    }),
    contentString,
    youHereInfowindow;
    infowindow = new google.maps.InfoWindow();

    if(youHere){
      contentString = "<p>You are here</p>";
      iconFile = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'; 
      marker.setIcon(iconFile);
      marker.setAnimation(google.maps.Animation.BOUNCE);

      youHereInfowindow = new google.maps.InfoWindow({
        content: contentString
      });
      youHereInfowindow.open(map, marker);
    }else{
      contentString = "<p><b>"+storeDataObj.mallName+"</b><br/>"+storeDataObj.streetAddress+"<br/>"+storeDataObj.city+", "+storeDataObj.state+"-"+storeDataObj.postalCode+
                      "<br>"+storeDataObj.phone+"</p>";

      marker.addListener('click', function() {
        infowindow.setContent(contentString);
        infowindow.open(map, marker);

        //Directions
        var directionsDisplay = new google.maps.DirectionsRenderer();
        var directionsService = new google.maps.DirectionsService();
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById('right-panel'));

        calculateAndDisplayRoute(directionsService,directionsDisplay, storeDataObj);
      }); 
    }

    bounds.extend(marker.getPosition());
  }

  function calculateAndDisplayRoute(directionsService, directionsDisplay, storeDataObj) {
    
    directionsService.route({
      origin: new google.maps.LatLng(geolocationPosition.coords.latitude,geolocationPosition.coords.longitude),
      destination: new google.maps.LatLng(parseFloat(storeDataObj.latitudeDeg),parseFloat(storeDataObj.longitudeDeg)),
      travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
}

  