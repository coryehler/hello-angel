function searchProducts() {
	console.log("Search products");
}

function ProductOptions(words) {

	for(var i=0; i<words.length; i++){
		if(words[i].search("show") != -1){
			$("#product_options").removeClass("hide").addClass('show');
			//AgentSpeak("Displaying Filters");
			break;
		}else if(words[i].search("hide") != -1){
			$("#product_options").removeClass("show").addClass('hide');
			$("#angelsearchresults").html("");
			//AgentSpeak("Displaying Filters");
			break;
		}
	}
	
}

function SelectOptions(words) {
	console.log("SelectOptions");
}

function SelectBrand(words) {
	var filterStatus,
	voiceSearchText = words.join(' ').toLowerCase();

	if($("#product_options").hasClass('hide')){
		AgentSpeak("Say command show products options to show the options first"); 
	}else {
	 	for(var j=0; j<words.length; j++) {
	 		if(words[j].toLowerCase().search("add") != -1 || words[j].toLowerCase().search("select") != -1){
	 			filterStatus = "add";
	 			break;
	 		}else if(words[j].toLowerCase().search("remove") != -1){
	 			filterStatus = "remove";
	 			break;
	 		}
	 	}

	 	for(var i=0; i<words.length; i++) {
	 		$("#select_brand > option").each(function() {
			    if( (words[i].toLowerCase().search(this.text.toLowerCase()) != -1) || (voiceSearchText.search(this.text.toLowerCase()) != -1) ){
			    	if(filterStatus === "add"){
			    		$("#select_brand option[value='"+this.value+"']").prop("selected", "selected").trigger('change');
			    	}else if(filterStatus === "remove"){
			    		$("#select_brand option[value='"+this.value+"']").prop("selected", false).trigger('change');
			    	}		    	
			    }
			});
	 	}
	}

	AgentSpeak("Filter is updated");
}

function SelectCategory(words) {
	var filterStatus;

	if($("#product_options").hasClass('hide')){
		AgentSpeak("Say command show products options to show the options first"); 
		return;
	}else {
	 	for(var j=0; j<words.length; j++) {
	 		if(words[j].toLowerCase().search("add") != -1){
	 			filterStatus = "add";
	 			break;
	 		}else if(words[j].toLowerCase().search("remove") != -1){
	 			filterStatus = "remove";
	 			break;
	 		}
	 	}

	 	for(var i=0; i<words.length; i++) {
	 		$("#select_category > option").each(function() {
			    //console.log(this.text + ' ' + this.value);
			    if(words[i].toLowerCase().search(this.value.toLowerCase()) != -1 ){
			    	if(filterStatus === "add"){
			    		$("#select_category option[value="+this.value+"]").prop("selected", "selected").trigger('change');
			    	}else if(filterStatus === "remove"){
			    		$("#select_category option[value="+this.value+"]").prop("selected", false).trigger('change');
			    	}		    	
			    }
			});
	 	}
	}
	AgentSpeak('Filter is updated');

}

//ADD/Remove/Clear filters
function Filters(words) {
	console.log("Filters");

	for(var i=0; i<words.length; i++){
		if(words[i].search("show") != -1){
			$("#product_options").removeClass("hide").addClass('show');
			$("#angelsearchresults").html("");
			AgentSpeak("Displaying Filters");
			break;
		}else if(words[i].search("hide") != -1){
			$("#product_options").removeClass("show").addClass('hide');
			$("#angelsearchresults").html("");
			AgentSpeak("Hiding Filters");
			break;
		}else if(words[i].search("clear") != -1){
			$('#select_brand').find('option:selected').prop("selected", false).trigger('change');
			$('#select_color').find('option:selected').prop("selected", false).trigger('change');
			$('#select_category').find('option:selected').prop("selected", false).trigger('change');
			$('#select_sort').find('option:selected').prop("selected", false);
			AgentSpeak("Filters have been removed");
			break;
		}
	}

}

function FilterColor(words) {
	var filterStatus;

	if($("#product_options").hasClass('hide')){
		AgentSpeak("Say command show products options to show the options first"); 
	}else {
	 	for(var j=0; j<words.length; j++) {
	 		if(words[j].toLowerCase().search("add") != -1 || words[j].toLowerCase().search("select") != -1 ){
	 			filterStatus = "add";
	 			break;
	 		}else if(words[j].toLowerCase().search("remove") != -1){
	 			filterStatus = "remove";
	 			break;
	 		}
	 	}

	 	for(var i=0; i<words.length; i++) {
	 		$("#select_color > option").each(function() {
			    if(words[i].toLowerCase().search(this.text.toLowerCase()) != -1 ){
			    	if(filterStatus === "add"){
			    		$("#select_color option[value='"+this.value+"']").prop("selected", "selected").trigger('change');
			    	}else if(filterStatus === "remove"){
			    		$("#select_color option[value='"+this.value+"']").prop("selected", false).trigger('change');
			    	}		    	
			    }
			});
	 	}
	}

	AgentSpeak("Filter is updated");
}

function ShowProducts(words){
	var filterWordMatch = searchFilterWords(words),
		products,
		fillteredProd = [];

	if(!filterWordMatch){
		displayProducts(productData.data);
		AgentSpeak(productData.data.length + "Products are displayed");
	}else{
		fillteredProd = filterProducts();
		displayProducts(fillteredProd);
		AgentSpeak(fillteredProd.length + "Products are displayed");
	}
	

}

$('#select_brand').change(function() {
    displayProducts(filterProducts());
});

 $('#select_color').change(function() {
    displayProducts(filterProducts());
});

$('#select_category').change(function() {
    displayProducts(filterProducts());
});


 function displayProducts(products){
	$('#angelsearchresults').empty();
	$('#angelsearchresults').append("<div id='totalProducts'>Total Products: "+products.length+"</div>");
	$('#angelsearchresults').append("<div id='products'></div>");
	var contentString = "";

	sortByProducts(products);

	for(var i=0; i< products.length; i++) {
		contentString += "<div class='product'>";
		contentString += "<img src='"+products[i].image+"' width='150'>";
		contentString += "<p>"+products[i].name+"<br/>";
		contentString += "Brand:"+products[i].brand+"<br/>";
		contentString += "Category:"+products[i].category+"<br/>";
		contentString += "Color:"+products[i].color+"<br/>";
		contentString += "Price:"+products[i].price+"<br/>";
		contentString += "Rating:"+products[i].rating+"</p>";
		contentString += "</div>";
	}
	$('#products').append(contentString);

	//AgentSpeak("Found "+products.length+" products");
 }

 function filterProducts() {
 	var brandSearchResults = [],
    	colorSearchResults = [],
    	categorySearchResults = [],
    	newSearchData = [],
    	brandFilters = $('#select_brand').val(),
    	colorFilters = $('#select_color').val(),
    	categoryFilters = $('#select_category').val();

    newSearchData = productData.data;

    if(brandFilters && brandFilters.length > 0){
    	//filter by brand
	    for(var i=0; i< brandFilters.length; i++){
			$.grep(newSearchData, function (element, index) {
			 	if (String(element.brand).toLowerCase() === String(brandFilters[i]).toLowerCase()) {
			 		brandSearchResults.push(element);
			 	}
			});
		}

		if(brandSearchResults && brandSearchResults.length > 0){
	    	newSearchData = brandSearchResults;
	    }else {
	    	newSearchData = [];
	    }
    }

   
 	
	if(colorFilters && colorFilters.length > 0){
		//filter by color
	    for(var j=0; j< colorFilters.length; j++){
			$.grep(newSearchData, function (element, index) {
			 	if (String(element.color).toLowerCase() === String(colorFilters[j]).toLowerCase()) {
			 		colorSearchResults.push(element);
			 	}
			});
		}

		if(colorSearchResults && colorSearchResults.length > 0){
	    	newSearchData = colorSearchResults;
	    }else {
	    	newSearchData = [];
	    }
	}

	

	if(categoryFilters && categoryFilters.length > 0){
		//filter by color
	    for(var k=0; k< categoryFilters.length; k++){
			$.grep(newSearchData, function (element, index) {
			 	if (String(element.category).toLowerCase() === String(categoryFilters[k]).toLowerCase()) {
			 		categorySearchResults.push(element);
			 	}
			});
		}

		if(categorySearchResults && categorySearchResults.length > 0){
	    	newSearchData = categorySearchResults;
	    }else {
	    	newSearchData = [];
	    }
	}

    return newSearchData;
 }

 function SortProducts(words) {
 	var searchText = words.join(' ').toLowerCase(),
 		filteredData,
 		sortbyText ="" ;

	if($("#product_options").hasClass('hide')){
		AgentSpeak("Say command show products options to show the options first"); 
		return;
	}else {
	 		$("#select_sort > option").each(function() {
			    if(searchText.search(this.text.toLowerCase())  != -1 ){
			    	sortbyText = this.value;
			    	$("#select_sort option[value='"+this.value+"']").prop("selected", "selected");
			    }else{
			    	$("#select_sort option[value='"+this.value+"']").prop("selected", false);
			    }
			});
	}

	filteredData = filterProducts();
	displayProducts(filteredData);

	AgentSpeak(filteredData.length+" Products are sorted by "+sortbyText);
 }

 function sortByProducts(filteredData){
 	var sortAscending = true,
 		sortProperty;

 	if($( "#select_sort" ).find(':selected').attr('data-sort') === "desc"){
		sortAscending = false;
	}

	sortProperty = $("#select_sort").find(':selected').attr('data-value');


	if($( "#select_sort" ).find(':selected').length > 0){
		sortArrOfObjectsByParam(filteredData, sortProperty,sortAscending);
	}
 	
 }

function sortArrOfObjectsByParam(arrToSort , strObjParamToSortBy , sortAscending) {
    if(sortAscending === undefined) sortAscending = true;  // default to true
    
    if(sortAscending) {
        arrToSort.sort(function (a, b) {
            return parseFloat(a[strObjParamToSortBy]) > parseFloat(b[strObjParamToSortBy]);
        });
    }
    else {
        arrToSort.sort(function (a, b) {
            return parseFloat(a[strObjParamToSortBy]) < parseFloat(b[strObjParamToSortBy]);
        });
    }
}

function searchFilterWords(words) {
	var voiceSearchText = words.join(' ').toLowerCase(),
		filterWordMatch= false;

	$('#select_brand option').each(function(){
		if(voiceSearchText.search($(this).val().toLowerCase()) != -1){
			$("#select_brand option[value='"+this.value+"']").prop("selected", "selected");
			filterWordMatch = true;
		}
    });

    $('#select_color option').each(function(){
		if(voiceSearchText.search($(this).val().toLowerCase()) != -1){
			$("#select_color option[value='"+this.value+"']").prop("selected", "selected");
			filterWordMatch = true;
		}
    });

    $('#select_category option').each(function(){
		if(voiceSearchText.search($(this).val().toLowerCase()) != -1){
			$("#select_category option[value='"+this.value+"']").prop("selected", "selected");
			filterWordMatch = true;
		}
    });


	return filterWordMatch;

}
