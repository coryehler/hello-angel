// modules
var _ = require('lodash');
var autoprefixer = require('gulp-autoprefixer');
var browserify = require('browserify');
var browserSync = require('browser-sync');
var concat = require('gulp-concat');
var csso = require('gulp-csso');
var del = require('del');
var fabricate = require('gulp-fabricate');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var gutil = require('gulp-util');
var imagemin = require('gulp-imagemin');
var jshint = require('gulp-jshint');
var path = require('path');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var source = require('vinyl-source-stream');
var streamify = require('gulp-streamify');
var streamqueue = require('streamqueue');
var stylish = require('jshint-stylish');
var uglify = require('gulp-uglify');


// config defaults
var defaults = {
	templates: {
		src: 'src/templates/pages/**/*',
		dest: 'dist/',
		watch: ['src/templates/**/*', 'src/data/**/*.json'],
		options: {}
	},
	scripts: {
		src: './src/assets/scripts/main.js',
		dest: 'dist/assets/scripts',
		watch: 'src/assets/scripts/main.js'
	},
	styles: {
		src: 'src/assets/styles/main.scss',
		dest: 'dist/assets/styles',
		watch: 'src/assets/styles/**/*',
		browsers: ['last 2 version']
	},
	fonts: {
		src: 'src/assets/fonts/**/*',
		dest: 'dist/assets/fonts',
		watch: 'src/assets/fonts/**/*'
	},
	images: {
		src: 'src/assets/images/**/*',
		dest: 'dist/assets/images',
		watch: 'src/assets/images/**/*'
	}
};

// merge user with default config
var config = _.merge(defaults, require('./package.json').config);
config.dev = gutil.env.dev;


// clean
gulp.task('clean', function (cb) {
	del(['dist'], cb);
});


// templates
gulp.task('templates', function () {
	return gulp.src(config.templates.src)
		.pipe(fabricate(config.templates.options))
		.on('error', function (error) {
			gutil.log(gutil.colors.red(error));
			this.emit('end');
		})
		.pipe(gulp.dest(config.templates.dest));
});


// scripts
gulp.task('scripts', function () {

	var filename = path.basename(config.scripts.src);

	var main = function () {
		return browserify(config.scripts.src).bundle()
			.on('error', function (error) {
				gutil.log(gutil.colors.red(error));
				this.emit('end');
			})
			.pipe(source(filename));
	};

	return streamqueue({ objectMode: true }, main())
		.pipe(streamify(concat(filename)))
		.pipe(gulpif(!config.dev, streamify(uglify())))
		.pipe(gulp.dest(config.scripts.dest));

}); 

gulp.task('jshint', function (cb) {
	return gulp.src(config.scripts.watch)
		.pipe(jshint({loopfunc:true}))
		.pipe(jshint.reporter(stylish))
		.pipe(jshint.reporter('fail'))
		.on('error', cb);
});

// styles
gulp.task('styles', function () {
	return gulp.src(config.styles.src)
		.pipe(sass({
			errLogToConsole: true
		}))
		.pipe(csso())
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest(config.styles.dest));
});

// fonts
gulp.task('fonts', function () {
	return gulp.src(config.fonts.src)
		.pipe(gulp.dest(config.fonts.dest));
});

// images
gulp.task('images', function () {
	return gulp.src(config.images.src)
		.pipe(imagemin({
			progressive: true,
			interlaced: true
		}))
		.pipe(gulp.dest(config.images.dest));
});

// server
gulp.task('browser-sync', function () {
	browserSync({
		server: {
			baseDir: config.templates.dest
		},
		notify: false,
		logPrefix: 'BrowserSync'
	});
});


// watch
gulp.task('watch', ['browser-sync', 'default'], function () {
	gulp.watch(config.images.watch, ['images', browserSync.reload]);
	gulp.watch(config.scripts.watch, ['scripts', browserSync.reload]);
	gulp.watch(config.styles.watch, ['styles', browserSync.reload]);
	gulp.watch(config.templates.watch, ['templates', browserSync.reload]);
});


// default build task
gulp.task('default', ['jshint'], function () {

	// define build tasks
	var tasks = [
		'templates',
		'scripts',
		'styles',
		'images',
		'fonts'
	];

	// run build
	runSequence(tasks, function () {
		if (config.dev) {
			gulp.start('watch');
		}
	});

});
